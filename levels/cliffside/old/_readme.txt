Now that we have your attention, let's read something relevant...

Track Name--- Cliffside

Author---         R6TurboExtreme and Dave-o-rama

Type---    Extreme

Length---    351m

Directions---    Standard stuff... Just unzip in the main Re-volt folder, then everything should be set.

Description---  This is a mighty fine island, with a cliff on it, and three guesses, the cars are going to drive on it!
Alot of vegetation, (hopefully) alot of detail, and everything a human needs. So, don't read this, but DRIVE!

Re-Volt 1.1 is obsolete--- You need the 1.2 update for Re-Volt if you have any hopes of running this track properly. Go to  rv12.zackattackgames.net for Re-Volt 1.2. It is highly recommended that you download and install the 12.0405 alpha release for full compatibility with this track.

The work done:
R6 did the mesh and a few plants, AI and a few makeitgood stuffs
Dave did the instances (plants etc) and camera nodes

Thank You To:
You for downloading
Acclaim, for Re-Volt
Jigebren, for WolfR4
huki and jigebren for Re-Volt 1.2
BurnRubr for the cabin
JimK for the offroad kit
Urnemanden for the palmtree .prm
Dave-o-rama for being too lazy for his own fucking good :)
ali & co. for rvglue
srmalloy for prmbend
The guys who made Rv-remap, rv-sizer, etc.

Our websites -
re-voltcars.webs.com   (R6)
daveorama.webs.com  (Dave)

Copyrights\Permissions--- If you wanna use something, just go ahead and use it. Just mention us in the readme. Please? ;)