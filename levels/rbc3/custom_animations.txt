{
;==============================================================================
;  RVGL CUSTOM ANIMATIONS
;
;  This file allows creating custom object animation (moving whole meshes):
;  - Up to 256 different animations can be defined.
;  - Each has a main body part and up to 15 child bones which follow their parent bone.
;  - Every animation can have up to 256 keyframes, defining the bone positions at that time.
;    The game then interpolates the animation between those keyframes.
;  - Up to 64 custom model slots are supported.
;
;  The type of interpolation can be one of the following:
;    0: linear. Constant velocity and angular velocity.
;    1: smooth start, sudden end. Like an object starting to fall, then hitting the ground.
;    2: sudden start, smooth end.
;    3: both smooth start and end.
;    4: exaggerating or overshooting, i.e. first moving a bit in the opposite direction,
;       then over the target amount, then again backwards. Gives a robotic look.
;==============================================================================

;==============================================================================
;  ANIMATION
;==============================================================================
;  Possible options that can be defined within an animation block, and their 
;  default values are given below:
;
;  ANIMATION {
;    Slot                      0                         ; Slot ID (0 to 255)
;    Name                      "DEFAULT"                 ; Display name (currently unused)
;    Mode                      0                         ; 0: loop / 1: animate a single time, stopping after the last frame / 2: back and forth (reverse after last keyframe)
;    NeedsTrigger              false                     ; Whether the animation needs to be triggered
;    PreCountdown              false                     ; Whether the animation must start before countdown (in multiplayer)
;
;    BONE {
;      BoneID                  0                         ; Body part ID (0 to 15); part 0 is the main body part
;      ModelID                 -1                        ; Model ID (0 to 63, or -1)
;      Parent                  0                         ; Body part parent ID; must be smaller than the bone ID; not present at the main body part
;      OffsetTranslation       0.000 0.000 0.000         ; Offset to parent location
;      OffsetRotationAxis      0.000 -1.000 0.000        ; Offset rotation axis
;      OffsetRotationAmount    0.000                     ; Offset rotation (in degrees)
;    }
;
;    KEYFRAME {
;      FrameNr                 0                         ; Number of the keyframe (0 to 255)
;      Time                    1.000                     ; Time (in seconds) since previous keyframe
;      Type                    0                         ; Interpolation type. 0 - linear, 1 - smooth start, 2 - smooth end, 3 - both smooth, 4 - overshoot
;      BONE {
;        BoneID                0                         ; Body part ID
;        Visible               true                      ; Toggle body part visibility (for all future keyframes until it is changed again)
;        Translation           0.000 0.000 0.000         ; Translation
;        RotationAxis          0.000 -1.000 0.000        ; Rotation axis
;        RotationAmount        0.000                     ; Rotation (in degrees)
;      }
;    }
;  }
;==============================================================================
;==============================================================================

;--------
; Models
;--------

MODEL   0   "thwomp_rising"
MODEL   1   "thwomp_waking"
MODEL   2   "thwomp_falling"
MODEL   3   "thwomp_shadow"
MODEL   4   "fireball"
MODEL   5   "fireshell1"

;--------
; Sounds
;--------

SFX	0	"thwomp_shaking_sfx"
SFX	1	"thwomp_impact_sfx"
SFX	2	"fire_sfx"

  ANIMATION {
	Slot                      0                         ; Slot ID (0 to 255)
	Name                      "THWOMP LOOP"                 ; Display name (currently unused)
	Mode                      0                         ; 0: loop / 1: animate a single time, stopping after the last frame / 2: back and forth (reverse after last keyframe)
	NeedsTrigger              false                     ; Whether the animation needs to be triggered
	PreCountdown              false                     ; Whether the animation must start before countdown (in multiplayer)

	BONE {
	  BoneID                  0                         ; Body part ID (0 to 15); part 0 is the main body part
	  ModelID                 0                        ; Model ID (0 to 63, or -1)
	}
	
	BONE {
	  BoneID			   1
	  ModelID			   1
	}
	
	BONE {
	  BoneID			   2
	  ModelID			   2
	}

	KEYFRAME {
	  FrameNr                 0                         ; Number of the keyframe (0 to 255)
	  Time                    0.000                     ; Time (in seconds) since previous keyframe
	  Type                    0                         ; Interpolation type. 0 - linear, 1 - smooth start, 2 - smooth end, 3 - both smooth, 4 - overshoot
	  BONE {
		BoneID                0                         ; Body part ID
		Visible               true                      ; Toggle body part visibility (for all future keyframes until it is changed again)
		Translation           0.000 0.000 0.000         ; Translation
	  }
	  BONE {
		BoneID                1                         ; Body part ID
		Visible               false                      ; Toggle body part visibility (for all future keyframes until it is changed again)
		Translation           0.000 0.000 0.000         ; Translation
	  }
	  BONE {
		BoneID                2                         ; Body part ID
		Visible               false                      ; Toggle body part visibility (for all future keyframes until it is changed again)
		Translation           0.000 0.000 0.000         ; Translation
	  }
	}
	
	KEYFRAME {
	  FrameNr                 1                         ; Number of the keyframe (0 to 255)
	  Time                    3.000                     ; Time (in seconds) since previous keyframe
	  Type                    0                         ; Interpolation type. 0 - linear, 1 - smooth start, 2 - smooth end, 3 - both smooth, 4 - overshoot
	  BONE {
		BoneID                0                         ; Body part ID
		Visible               true                      ; Toggle body part visibility (for all future keyframes until it is changed again)
		Translation           0.000 -500.000 0.000         ; Translation
	  }
	}

	KEYFRAME {
	  FrameNr                 2                         ; Number of the keyframe (0 to 255)
	  Time                    1.000                     ; Time (in seconds) since previous keyframe
	  Type                    0                         ; Interpolation type. 0 - linear, 1 - smooth start, 2 - smooth end, 3 - both smooth, 4 - overshoot
	  BONE {
		BoneID                0                         ; Body part ID
		Visible               true                      ; Toggle body part visibility (for all future keyframes until it is changed again)
		Translation           0.000 0.000 0.000         ; Translation
	  }
	}
	
	KEYFRAME {
	  FrameNr                 3                         ; Number of the keyframe (0 to 255)
	  Time                    0.1                     ; Time (in seconds) since previous keyframe
	  Type                    4                         ; Interpolation type. 0 - linear, 1 - smooth start, 2 - smooth end, 3 - both smooth, 4 - overshoot
	  BONE {
		BoneID			   0
		Visible			   false
	  }
	  BONE {
		BoneID                1                         ; Body part ID
		Visible               true                      ; Toggle body part visibility (for all future keyframes until it is changed again)
		Translation           0.000 -25.000 0.000         ; Translation
		SFX {
	  		SfxID				0
	  		Range				25
	  		Looping			false
	     }
	  }
	}
  
	KEYFRAME {
	  FrameNr                 4                         ; Number of the keyframe (0 to 255)
	  Time                    0.1                    ; Time (in seconds) since previous keyframe
	  Type                    4                         ; Interpolation type. 0 - linear, 1 - smooth start, 2 - smooth end, 3 - both smooth, 4 - overshoot
	  BONE {
		BoneID                1                         ; Body part ID
		Visible               true                      ; Toggle body part visibility (for all future keyframes until it is changed again)
		Translation           0.000 25.000 0.000         ; Translation
	  }
	}

	KEYFRAME {
	  FrameNr                 5                         ; Number of the keyframe (0 to 255)
	  Time                    0.1                     ; Time (in seconds) since previous keyframe
	  Type                    0                         ; Interpolation type. 0 - linear, 1 - smooth start, 2 - smooth end, 3 - both smooth, 4 - overshoot
	  BONE {
		BoneID                1                         ; Body part ID
		Visible               true                      ; Toggle body part visibility (for all future keyframes until it is changed again)
		Translation           0.000 -25.000 0.000         ; Translation
	  }
	}

	KEYFRAME {
	  FrameNr                 6                         ; Number of the keyframe (0 to 255)
	  Time                    0.1                     ; Time (in seconds) since previous keyframe
	  Type                    0                         ; Interpolation type. 0 - linear, 1 - smooth start, 2 - smooth end, 3 - both smooth, 4 - overshoot
	  BONE {
		BoneID                1                         ; Body part ID
		Visible               true                      ; Toggle body part visibility (for all future keyframes until it is changed again)
		Translation           0.000 25.000 0.000         ; Translation
	  }
	}

	KEYFRAME {
	  FrameNr                 7                         ; Number of the keyframe (0 to 255)
	  Time                    0.1                     ; Time (in seconds) since previous keyframe
	  Type                    0                         ; Interpolation type. 0 - linear, 1 - smooth start, 2 - smooth end, 3 - both smooth, 4 - overshoot
	  BONE {
		BoneID                1                         ; Body part ID
		Visible               true                      ; Toggle body part visibility (for all future keyframes until it is changed again)
		Translation           0.000 -25.000 0.000         ; Translation
	  }
	}
  
	KEYFRAME {
	  FrameNr                 8                         ; Number of the keyframe (0 to 255)
	  Time                    0.150                     ; Time (in seconds) since previous keyframe
	  Type                    0                         ; Interpolation type. 0 - linear, 1 - smooth start, 2 - smooth end, 3 - both smooth, 4 - overshoot
	  BONE {
		BoneID			   1
		Visible			   false
	  }
	  BONE {
		BoneID                2                         ; Body part ID
		Visible               true                      ; Toggle body part visibility (for all future keyframes until it is changed again)
		Translation           0.000 500.000 0.000         ; Translation
	  }
	}
	
	KEYFRAME {
	  FrameNr				9
	  Time				0.1
	  Type				0
	  BONE {
		BoneID                2                         ; Body part ID
		Visible               true                      ; Toggle body part visibility (for all future keyframes until it is changed again)
		Translation           0.000 0.000 0.000         ; Translation
		SPARK {
			Type			31
			AverageSpeed	-100
			VariableSpeed	200
			Frequency		150
		}
	  }
	}
			
	KEYFRAME {
	  FrameNr                 10                         ; Number of the keyframe (0 to 255)
	  Time                    1.500                     ; Time (in seconds) since previous keyframe
	  Type                    0                         ; Interpolation type. 0 - linear, 1 - smooth start, 2 - smooth end, 3 - both smooth, 4 - overshoot
	  BONE {
		BoneID                2                         ; Body part ID
		Visible               true                      ; Toggle body part visibility (for all future keyframes until it is changed again)
		Translation           0.000 0.000 0.000         ; Translation
		SFX {
			SfxID			1
			Range			25
			Looping		false
		}
		SPARK {
			Type			-1
		}
	  }
	}
  }
  
  ANIMATION {
	Slot                      1                         ; Slot ID (0 to 255)
	Name                      "THWOMP SHADOW STATIC"                 ; Display name (currently unused)
	Mode                      0                         ; 0: loop / 1: animate a single time, stopping after the last frame / 2: back and forth (reverse after last keyframe)
	NeedsTrigger              false                     ; Whether the animation needs to be triggered
	PreCountdown              false                     ; Whether the animation must start before countdown (in multiplayer)

	BONE {
	  BoneID                  0                         ; Body part ID (0 to 15); part 0 is the main body part
	  ModelID                 3                        ; Model ID (0 to 63, or -1)
	}
	
	KEYFRAME {
	  FrameNr			   0
	  Time				   1.000
	  Type				   0
	  BONE {
		BoneID			   0
		Visible			   true
	  }
	}
  }
  
  ANIMATION {
	Slot                      2                         ; Slot ID (0 to 255)
	Name                      "FIREBALL LOOP"                 ; Display name (currently unused)
	Mode                      0                         ; 0: loop / 1: animate a single time, stopping after the last frame / 2: back and forth (reverse after last keyframe)
	NeedsTrigger              false                     ; Whether the animation needs to be triggered
	PreCountdown              false                     ; Whether the animation must start before countdown (in multiplayer)

	BONE {
	  BoneID                  0                         ; Body part ID (0 to 15); part 0 is the main body part
	  ModelID                 4                        ; Model ID (0 to 63, or -1)
	}
	
	KEYFRAME {
	  FrameNr			   0
	  Time				   3.000
	  Type				   0
	  BONE {
		BoneID			   0
		Visible			   true
	  }
	}
	
	KEYFRAME {
	  FrameNr			   1
	  Time				   1.000
	  Type				   2
	  BONE {
		BoneID			   0
		Translation		   0.000 -800.000 0.000
		RotationAxis	   	   1.000 -1.000 1.000
		RotationAmount	        1080
		SFX {
			SfxID			2
			Range			25
			Loop			false
		}
		SPARK {
			Type			32
			AverageSpeed	0
			VariableSpeed	0
			Frequency		200
		}
	  }
	}
	
	KEYFRAME {
	  FrameNr			   2
	  Time				   1.000
	  Type				   1
	  BONE {
		BoneID			   0
		Translation		   0.000 800.000 0.000
		RotationAxis	   -1.000 1.000 -1.000
		RotationAmount	   1080
	  } 
	}
  }
  
  ANIMATION {
	Slot                      3                         ; Slot ID (0 to 255)
	Name                      "FIRESHELL LOOP"                 ; Display name (currently unused)
	Mode                      0                         ; 0: loop / 1: animate a single time, stopping after the last frame / 2: back and forth (reverse after last keyframe)
	NeedsTrigger              false                     ; Whether the animation needs to be triggered
	PreCountdown              false                     ; Whether the animation must start before countdown (in multiplayer)

	BONE {
	  BoneID                  0                         ; Body part ID (0 to 15); part 0 is the main body part
	  ModelID                 5                        ; Model ID (0 to 63, or -1)
	}
	
	KEYFRAME {
	  FrameNr			   0
	  Time				   3.000
	  Type				   0
	  BONE {
		BoneID			   0
		Visible			   true
	  }
	}
	
	KEYFRAME {
	  FrameNr			   1
	  Time				   1.000
	  Type				   2
	  BONE {
		BoneID			   0
		Visible			   true
		Translation		   0.000 -800.000 0.000
		RotationAxis	   0.000 -1.000 0.000
		RotationAmount	   360
	}

	KEYFRAME {
	  FrameNr			   2
	  Time				   0.000
	  Type				   0
	  BONE {
		BoneID			   0
		Visible			   true
		Translation		   0.000 0.000 0.000
		RotationAxis	   0.000 0.000 -1.000
		RotationAmount	   180
	  }
	}

	KEYFRAME {
	  FrameNr			   3
	  Time				   1.000
	  Type				   1
	  BONE {
		BoneID			   0
		Visible			   true
		Translation		   0.000 -800.000 0.000
		RotationAxis	   0.000 -1.000 0.000
		RotationAmount     360
	  }
	}
  }
}