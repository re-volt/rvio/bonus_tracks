				         ____          _    __      ____    
				        / __ \___     | |\ / /___  / / /_   
				       / /_/ / _ \    | ||/ / __ \/ / __/\  
				      / _, _/  __/\ / | |/ / /_/ / / /__\/  
				     /_/ |_|\___/ /   |___/\____/_/\__/\    
				     \_\/ \_\\__\/     \__\/\___\_\/\_\/    

	=============================================================================================
	"Floating World" made by RST		based on a hilaire9's idea		   March 2005
	=============================================================================================


	Type - Extreme track


	Dificulty - So Extreme !! you will need the brake XDDD


	Idea - from hilaire9


	Description: Race around a lake with islands of a floating village with so many and funny
		 short cuts and of course a lot of tricks to discover ;) are you sure you know
		 all the tricks ?


	Install: The current install allow to play the "lite version" of the track.
		 If you want race the "Extreme version" must click in "install Track.exe".
		 That exe file will replace "Toytanic 2" track without delete it. Don't worry
		 Toytanic 2 not will be delete because the installer make one backup.
		 For uninstall the extreme version and restore "Toytanic 2" track click again
		 in the same exe file.


	Tools: 	MAKEITGOOD edit mode
		3D Studio Max 4.02
		Photoshop 8 CS
		SoundForge 6.01


	Custom models: Fish, water bubble, watermill and duck


	3d sounds: six custom 3d sounds replacing Toytanic track sounds
		   watermill, frog, waterfall, Underwater & water bubbles, wind, water drop


	Polygons: over 19.000 without cutom models


	Website: http://s8.invisionfree.com/Revolt_RC_Racing


	Special notes about Floating World Extreme version !!
	-----------------------------------------------------
		Floating World Extreme version will be placed in the Toytanic 2 track. That is needed
		because the track use 3d sounds and custom models. The posibilities using the
		installer are so great because we can play Toytanic 2 and when the race is over
		all racers can install Floating World Extreme without exit the game. That give us the
		posibility of play several tracks placed in the same position.
		To play the Extreme version you must select Toytanic 2 track. You will know if is
		installed because the gfx image of the Toytanic 2 changed to the gfx of Floating World.

		When Floating World Extreme is installed you can play it in Time Trial mode as the
		Re-volt original levels. That is a cool thing we can't do in the custom tracks :(


	Special notes about the "Install Track.exe"
	-------------------------------------------
		Some Windows systems not have "deltree.exe" and "move.exe" dos commands. For this
		reason i include these files into the track folder, all are needed for the
		install/uninstall process.

		If "Rooftops 2003" version is installed, for security reasons the installer will not
		allow the install of Floating World Extreme. That is needed because both tracks use
		custom models. In that case you must uninstall first Rooftops 2003 version using its
		"install track.exe" file.

		Don't worry, if you try the reverse thing, that mean install Rooftops 2003 and then
		try to install Floating World Extreme, the installer of the Floating World will
		detect the presence in re-volt of the other track and the install procedure will
		not be allowed while you not uninstall first.


	Credits: Thanx to SuperTard, hilaire9, Sjampo, RTT-Pig@sque and Iguana Entertainment


	You can distribute floating world.exe but not change the format.


	Enjoy it !!