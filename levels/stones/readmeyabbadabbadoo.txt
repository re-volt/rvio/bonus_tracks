===============================================================================
                        *** TRACK INFORMATION ***
===============================================================================

TRACK NAME:		YABBA DABBA DOO
DATE:			Feb 2001
AUTHOR:			BK
TRACK TYPE:		All Instance
TRACK LENGTH:		518 m

===============================================================================

STORY:
A sunny afternoon - just a few thousand years ago... 
Fred and Barney met at the Bedrock drive inn. After some of these great 
bronto burgers and some more beer they wanted to have still more fun. 
They took their RC-Flintmobiles and started the race... 

DESCRIPTION:
A challenging track with a fast racingline and lots of fun. All cars should
work fine - but the track was not designed for the super fast cars. The AI is
really challenging on this track so you should not expect to arrive first until you 
know the track very well.  ;-)  
 
TIPS / HINTS:
There are three more or less hidden stars - but you will have to leave the racing line 
to get them.
Leaving the racing line is possible, but don�t expect this will lower your lap times. 
Cars which fit the FLINTSTONE theme will soon be available...

THANKS and CREDITS:
Textures are partially based on original acclaim textures. Some smaller texture parts 
are based on screenshots of "THE FLINTSTONES" cartoon series produced by William Hanna 
and Joseph Barbera.
Thanks for the great editing tools, tutorials and sites from Ali, Chaos, Gibbler, Spaceman 
and the RVA Team. 
Thanks for lots of critical comments and discussions with kilo during testing.

COPYRIGHT / PERMISSIONS:
You may distribute this track as long as you include this readme file. You are NOT 
allowed to sell this track or contents in any fashion.

TRACK INSTALLATION: 
Unzip with folders in your revolt directory. Start Re-Volt and have fun.