Information
-----------------
Track Name:	Ground N Smash 2
Length:		636 meters
Difficulty	Medium
Author:		Saffron


Description
-----------------
It's been two years and the abandoned road turned into something more... extreme.
This is my best attempt at an instance track yet, utilizing the Walls, the Offroad Kit, and some Acclaim PRMs to the next degree. Also different textures from the prequel of this track.
Miromiro did the AI for challenging offline races.
Also did visiboxes, camera nodes, realistic lighting... I hope all this will suffice. :)

There is one star, it is easy to find, but can be difficult to get.
Oh, and I also included a practice star if any of you Wolf users are intrested.


Bugs
-----------------
Low fps on my computer, around 35-50 fps most of the time. And my computer is crap so it will be higher for you.
If you do complain about low fps, try out Jailhouse Rock and shut the fuck up. I'm sick and tired of elitist bastards nit-picking on track makers like me.


Credits
-----------------
The Gluer for RvGlue, RvMinis
Jasc for Paint Shop Pro 7
Ekla for his Walls
JimK for the Offroad Kit
Miromiro for the AI Nodes
Srmalloy for TrackMK
Kajeuter for beta testing
Jig for WolfR4
Huki & Co. for RV 1.2
Acclaim for Re-Volt and the PRMs


Permissions
-----------------
This track may not be distributed anywhere else outside of Re-Volt Zone and Re-Volt I/O content packs.