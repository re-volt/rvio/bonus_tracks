/********************************************************************
 *                                                                  *
 *      .-. .-.    .-.    .-. .-.  .----. .-.  .----.  .-. .-.      *
 *      | | | |    | |    | { } | { {__   | | /  {}  \ |  `| |      *
 *      | | | `--. | `--. | {_} | .-._} } | | \      / | |\  |      *
 *      `-' `----' `----' `-----' `----'  `-'  `----'  `-' `-'      *
 *                                                                  *
 ********************************************************************/

/********************************************************************
 * GENERAL TRACK INFORMATION                                        *
 ********************************************************************/

Author : triple6s
       & dod1
Full name : Jeff Lilly
            Laurent DODIN
E-mail : charliecot1@inetone.net
         dod1@wanadoo.fr

Track name : ILLUSION
File name : illusion.zip
File size : 1,6 Mo
Length : 672 m
Polygons : 4870 polys
Description : you're going in a psychadelic world. There's no story.

/********************************************************************
 * INSTALLATION                                                     *
 ********************************************************************/

unzip illusion.zip with directories in your revolt folder.
run illusion-arrows-install.bat to install new arrows.

/********************************************************************
 * REVERSED MODE                                                    *
 ********************************************************************/

This track is playable in reversed mode. To play it in reversed mode
enter TRACKER as your name, go back and enter your real name. When you
choose the track, go to one of the original levels, hit the arrow
down, and then go to illusion screen and hit enter twice. Now, you
should be playing 'Illusion R' !

/********************************************************************
 * CREDITS                                                          *
 ********************************************************************/

    concept : triple6s and dod1
    layout : dod1
    textures : most of them by triple6s, the rest by dod1
               except the sheet f made by acclaim
    lights : dod1
    vizibox : triple6s
    objects : dod1
    instances : dod1
    ai nodes : dod1
    track zones : dod1
    triggers : dod1
    camera nodes : none
    farce fields : dod1
    erm, nothing to see here : dod1 (used for mirroring surfaces)
    pos nodes : dod1
    acoustic zones : none
    prms : dod1 (etoile.prm and start.prm)
           acclaim (the others)
    bat files : original made by skitch, modified by dod1

/********************************************************************
 * ADDITIONAL CREDITS                                               *
 ********************************************************************/

    acclaim for the game
    schloink and supertard : 3dsmax help
    ali : rvtmod4 (ase2w, ase2prm & mkmirror)
    everyone that encourage us, motive us to finish this track
    everyone from the chatroom (#revolt-chat on austnet servers)
    YOU because you downloaded this track : enjoy !!

/********************************************************************
 * VISIT                                                            *
 ********************************************************************/

http://www.rvarchive.com
http://www.racerpoint.com/revolt

/********************************************************************
 * COPYRIGHT/PERMISSION                                             *
 ********************************************************************/

You may distribute this track FOR FREE on your website or in any
format if u keep this file (illusion.zip) intact. You may not use
stuffs from this track for your own creation without the author
permission (see credits to know who is concerned).