readme of Cake 

release date: October 2017
author: Instant; AI improved by Kipy
compatible with: rvgl only
type: extreme
length: 450 meters
reversed mode: not available
practice star: not available
used tools: Blender, GIMP, makeitgood
zip and folder name: cake

contact me on Discord: Instant#4105

changelog:

2018/09/17
fixed collision bugs

2018/08/14
updated track zones
removed backup files

2017/10/14
improved AI
optimization: rescaled textures, added visiboxes
visual improvements: mapped more faces, added waterbox


special thanks: r6te